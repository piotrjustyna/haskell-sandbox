monadicAdd :: Monad m => m Int -> m Int -> m Int
monadicAdd mx my =
    mx >>= (\x -> my >>= (\y -> return (x + y)))

-- do notation of monadicAdd.
-- This works the same way as monadicAdd, just looks less intimidating.
moreReadableMonadicAdd :: Monad m => m Int -> m Int -> m Int
moreReadableMonadicAdd mx my = do
    x <- mx
    y <- my
    return (x + y)

main = do
    let firstCollection = [1]
    let secondCollection = [3,2,1]

    let result = monadicAdd firstCollection secondCollection
    print result

    let reasultOfAReadableAdd = moreReadableMonadicAdd firstCollection secondCollection
    print reasultOfAReadableAdd
